from matplotlib.ticker import Formatter

from labtools.common import round_places

class SiFormatter(Formatter):
    def __init__(self, places=0, factor=1, exp=False):
        self.places = places
        self.factor = factor
        self.exp = exp

    def format_data(self, value, pos=None):
        value *= self.factor
        if not self.exp:
            return r"\num{{{:s}}}".format(round_places(value, self.places))
        else:
            return r"\num{{{:.{:d}e}}}".format(value, self.places)

    __call__ = format_data
