from decimal import Decimal
from math import floor, log10
import numpy

from labtools.common import round_figures, round_places, round_to, stds, vals
from labtools.table.common import table_format
from labtools.table.column import Column
from labtools.table.row import Row
from labtools.table.table import Table

class SiColumn(Column):
    def __init__(self, cells, header=None, footer=None):
        Column.__init__(self, [], "", header, footer, columns=1)
        for c in cells:
            self.add_cell(c)

    def add_cell(self, cell):
        Column.add_cell(self, cell)
        self._alignment = "S[table-format={:s}]".format(table_format([d for c in self._cells for d in c.data()]))

class SymbolRowTable(Table):
    def __init__(self):
        Table.__init__(self, "c c")
        self._columns.append(SiColumn([]))

    def add_si_row(self, symbol, data, unit="", figures=None, places=None, exp="", error=None):
        if not isinstance(data, (list, tuple)):
            if error == None and not stds(data) == 0:
                error = float(stds(data))
                data = float(vals(data))
        else:
            error = None
        if figures == None and places == None:
            figures = 1
        if error != None:
            if figures != None:
                error = round_figures(error, figures)
            else:
                error = round_places(error, places)
            data = round_to(data, error)
        else:
            if figures != None:
                data = round_figures(data, figures)
            else:
                data = round_places(data, places)
        self.add_row(Row("$" + (symbol if error == None else r"\errPhantom{{{:s}}}".format(symbol)) + "$ & " + (r"\si{{{:s}}}".format(unit) if exp == "" else r"\SI{{{:s}}}{{{:s}}}".format(exp, unit))))
        self._columns[0].add_cell(data)
        if error != None:
            self.add_row(Row(r"$\err{{{:s}}}$".format(symbol) + " & " + (unit if exp == "" else r"\SI{{{:s}}}{{{:s}}}".format(exp, unit))))
            self._columns[0].add_cell(error)

class SymbolColumn(SiColumn):
    def __init__(self, symbol, data, unit="", figures=None, places=None, exp="", squeeze_unit=False):
        header = r"\multicolumn{{1}}{{c}}{{${:s}$}}".format(symbol)
        if unit != "" or exp != "":
            if unit != "":
                if exp != "":
                    h2 = r"\SI{{{:s}}}{{{:s}}}".format(exp, unit)
                else:
                    h2 = r"\si{{{:s}}}".format(unit)
            else:
                h2 = r"\num{{{:s}}}".format(exp)
            if squeeze_unit:
                header = [header, r"\multicolumn{{1}}{{c}}{{$/\:{:s}\hphantom{{\:/}}$}}".format(h2)]
            else:
                header = r"\multicolumn{{1}}{{c}}{{${:s}\:/\:{:s}$}}".format(symbol, h2)
        else:
            h2 = None
        if figures == None or isinstance(figures, int):
            figures = [figures] * len(data)
        if places == None or isinstance(places, int):
            places = [places] * len(data)
        cell = []
        for d, f, p in zip(data, figures, places):
            if not numpy.isfinite(d):
                cell.append('')
            else:
                if f != None:
                    cell.append(round_figures(d, f))
                elif p != None:
                    cell.append(round_places(d, p))
                else:
                    cell.append(round_places(d, 6))
        SiColumn.__init__(self, [cell], header)

class SymbolColumnTable(Table):
    def __init__(self):
        Table.__init__(self)
        self._rows.append(Row())
