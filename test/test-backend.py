#!/usr/bin/env python3

from labtools import *

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(0, 3, 10000)
ax.plot(x, 4 + 4 * np.cos(2 * x), "r-.", label="foobar")
ax.errorbar([0, 1, 2, 3], [1, 2, 4, 8], yerr=[0.5, 0.2, 1, 2], fmt="-x", label="blag")
circ = mpl.patches.Circle((0.5, 0.5), 0.25, transform=ax.transAxes, facecolor='yellow', alpha=0.5)
ax.add_patch(circ)
ax.set_xlabel(r"hihig \SI{5}{\meter} $\displaystyle \cfrac{\cfrac{1}{2}}{2}$")
ax.set_ylabel(r"$\displaystyle \frac{1}{2}$", rotation=45)
ax.legend()
fig.set_figwidth(4)
fig.set_figheight(4)
fig.tight_layout()
fig.savefig("build/test1.tex", bbox_inches='tight', pad_inches=0)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, projection="polar")
x = np.linspace(0, 2 * np.pi, 10000)
ax.plot(x, np.cos(x))
fig.savefig("build/test2.tikz")
