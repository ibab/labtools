#!/usr/bin/env python3

from labtools.table import *
from uncertainties import ufloat
import numpy as np

t = SymbolColumnTable()
t.add(SymbolColumn("x", [123, 0.01, np.nan, 2], "\meter", 2, None, "e4"))
t.add(SymbolColumn("x", [123, 1.234, 0.01], "\meter", None, 5, "e4"))
print(t.render())
open("build/test.tab", "w").write(t.render())

t = SymbolRowTable()
t.add_si_row("x", 5123, error=5)
t.add_si_row("y", 0.01234, r"\meter", "e4", error=0.001)
t.add_si_row("z_b", 10)
t.add_hrule()
t.add_si_row("M", ufloat((100, 0.1)))
print(t.render())
open("build/test2.tab", "w").write(t.render())
